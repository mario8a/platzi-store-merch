# Store Merch

_Proyecto desarrollado en el curso de React hooks de platzi en donde tenemos una interfaz de venta de mercancia donde los usuarios pueden seleccionar la mercancia que les guste y comprarla. Dentro de la aplicacion esta integrado paypal en modo de desarrollo. Actualmente esta basada en una tienda de juguetes 3d. Cuando se compra un producto se lanza una ventana emergente de paypal, seguido de esto se mostrara una pagina con los detalles de tu compra y un mapa con la direccion a donde se enviara el producto_

![Captura del proyecto](./.readme-static/homePage.jpg)

<!-- [Ver en netlif](https://naughty-tesla-05bc7a.netlify.app/) -->

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._


### Pre-requisitos 📋

_Que cosas necesitas para instalar el software y como instalarlas_

```
Node js
```

### Instalación 🔧

_Clona el repositorio y una vez clonado, instalá las dependencias necesarias ejecutando el siguiente comando:_

```
npm install
```

_Dentro del proyecto encontraras un archivo **env.example** ejemplo para poner las variables de entorno para poder correr el proyecto_

```
REACT_APP_CLIENT_ID
API_GOOGLE_MAPS
```

_Levanta el proyecto con el siguiente comando:_

```
npm start
```

## Construido con 🛠️

_Herramientas utilizadas en el proyecto_

* [React](https://es.reactjs.org/) - El framework web usado
* [React Router](https://reactrouter.com/) - Componentes de navegacion
* [Paypal developers](https://developer.paypal.com/home) - Paypal
* [Google maps](https://developers.google.com/maps?hl=es-419)  - Goooge Maps API




---
⌨️ con ❤️ por [Mario Ochoa](https://www.instagram.com/mario_8a_/) 😊