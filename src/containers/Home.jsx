import React from 'react';
import { Helmet } from 'react-helmet';
import Products from './../components/Products';
import { Banner } from './../components/Banner';

export const Home = () => {
   return (
      <>
         <Helmet>
            <title>Retratoys - Productos</title>
         </Helmet>
         <Banner />
         <Products />
      </>
   )
}
