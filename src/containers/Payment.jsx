import React, {useContext} from 'react';
import { PayPalButton } from 'react-paypal-button-v2';
import AppContext from './../context/AppContext';
import '../styles/components/Payment.css';
import { useHistory } from 'react-router-dom';

export const Payment = () => {

   const { state, addNewOrder } = useContext(AppContext);
   const { cart, buyer } = state;
   const history = useHistory();

   const paypalOtions = {
      clientId: process.env.REACT_APP_CLIENT_ID,
      intent: 'capture',
      currency: 'MXN'
    }

   const buttonStyles = {
      layout:  'vertical',
      color:   'blue',
      shape:   'rect',
      label:   'paypal'
    }

    const handlePaymentSuccess = (data) => {
      if (data.status === 'COMPLETED') {
        const newOrder = {
          buyer,
          product: cart,
          payment: data
        }
        addNewOrder(newOrder);
        history.push('/checkout/success')
      }
    }

   const handleSumTotal = () => {
      const reducer = (accumulator, currentValue) => accumulator + currentValue.price;
      const sum = cart.reduce(reducer, 0);
      return sum;
   }

   return (
      <div className="Payment">
         <div className="Payment-content">
            <h3>Resumen del pedido: </h3>
            { cart.map((item) => (
               <div className="Payment-item" key={item.id}>
                  <div className="Payment-element">
                     <h4> {item.title} </h4>
                     <span> ${item.price} </span>
                  </div>
               </div>
            )) }
            <div className="Payment-button">
               <PayPalButton
                   paypalOptions={paypalOtions}
                   buttonStyles={buttonStyles}
                   amount={handleSumTotal()}
                   onSuccess={data => handlePaymentSuccess(data)}
            />
            </div>
         </div>
      </div>
   )
}
