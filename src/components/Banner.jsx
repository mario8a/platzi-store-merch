import React from 'react';
import bannerImg from '../assets/img/portada-retratoys.png';

import '../styles/components/Banner.css';

export const Banner = () => {
   return (
      <div className="img-container">
         <img src={bannerImg} alt="bannerImg" />
      </div>
   )
}
