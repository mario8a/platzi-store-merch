import React, { useContext } from 'react';
import Product from './Product';
import AppContext from '../context/AppContext';
import '../styles/components/Products.css';

const Products = () => {
  const { products, addToCart } = useContext(AppContext);
  // console.log(useContext(AppContext)); //Esta funcion tiene todo el estado de la app asi como las funciones


  const handleAddToCart = product => () => {
    /**Esta funcion esta declarada en el custom hook, pero debido a que estamos haciendo un estado global,
     * podemos acceder a ella desde cualquier componente con ayuda del hook useContext*/
    addToCart(product)
  }

  return (
    <div className="Products">
      <div className="Products-items">
        {products.map(product => (
          <Product key={product.id} product={product} handleAddToCart={handleAddToCart} />
        ))}
      </div>
    </div>
  );
}

export default Products;