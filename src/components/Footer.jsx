import React from 'react'
import '../styles/components/Footer.css';

const Footer = () => {
   return (
      <div className="Footer">
         <p className="Footer-title">Mario8a</p>
         <p className="Footer-copy">Todos los Izquerdos reservados</p>
      </div>
   )
}

export default Footer
